@extends('layouts.main')

@section('content')
    <h1>Update OOwner's Info</h1>
    <form action="{{route('owner.update',$owner->id)}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Name</label>
            <input  type="text" name="name" value="{{$owner->name}}" class="form-control">
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input  type="email" name="email" value="{{$owner->email}}" class="form-control">
        </div>         

        <input type="submit" class="btn btn-primary" values="Edit">
        <a class="btn btn-warning" href="/owner">Cancel</a>
    </form>
    <br>
    <h2>Cars Owned</h2>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Reg Number</th>
            <th scope="col">Manufacturer</th>
            <th scope="col">Model</th>
            <th scope="col">Year Made</th>
        </tr>
        </thead>
        <tbody>
        <?php $i=1 ?>
        @foreach($cars as $car)
            <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$car->RegNum}}</td>
                <td>{{$car->Manufacturer}}</td>
                <td>{{$car->Model}}</td>
                <td>{{$car->YearMade}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

