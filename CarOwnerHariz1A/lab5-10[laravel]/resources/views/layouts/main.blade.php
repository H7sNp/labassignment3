<!doctype html>
<html lang="en">
<head>
    @include('includes.header')
</head>
<body>
    <div class="container">



        @include('includes.navbar')

        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <string>{{$message}}</strong>
            </div>
        @endif

        @if ($message = Session::get('delsuccess'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <string>{{$message}}</strong>
            </div>
        @endif





        @yield('content')

        @include('includes.footer')
    </div>
</body>
</html>