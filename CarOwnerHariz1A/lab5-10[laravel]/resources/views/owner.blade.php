@extends('layouts.main')

@section('content')

    <h1>Car Owner List</h1>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Add Owner
    </button>
    <br><br>


    <!--<a class="btn btn-outline-primary float-right"><ion-icon name="person-add"></ion-icon></a>-->
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Car</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @if ($i==0)
            <tr>
                <td colspan="5"> No Data </td>
            </tr>
        @else   
            @foreach($owners as $owner)
                <tr>
                    <th scope="row"> {{$i++}} </th>
                    <td>{{$owner->name}}</td>
                    <td>
                        <ion-icon name="mail"></ion-icon>
                        {{$owner->email}}
                        <br>
                    </td>

                    <td>
                        @foreach($owner->cars as $car)
                        <ion-icon name="car"></ion-icon>
                        {{$car->RegNum}} {{$car->Manufacturer}}
                        {{$car->Model}}<br>
                        @endforeach
                    </td>
                    <td>
                        <a href="{{route('owner.edit', $owner->id)}}"><ion-icon name="create"></ion-icon></a>
                        <a href="{{route('destroy1', $owner->id)}}"><ion-icon name="trash"></ion-icon></a>
                        <a href="{{route('owner.show', $owner->id)}}"><ion-icon name="analytics"></ion-icon></a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <!--MODAL CONTENT HERE!-->
      <div class="modal-body">
      <form action="/owner/create" method="POST">
      @csrf


  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Your Name">
    <small id="emailHelp" class="form-text text-muted">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Email</label>
    <input name="email" type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Your Email">
  </div>
  
  <button type="submit" class="btn btn-primary">Add</button>
</form>
      </div>
        
      <div  class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      <!--MODAL ENDS HERE!-->
@endsection




