@extends('layouts.main')

@section('content')

    <h1>Driver List</h1>
    <!--<a class="btn btn-outline-primary float-right"><ion-icon name="person-add"></ion-icon></a>-->
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">ICno</th>
            <th scope="col">HPno</th>
            <th scope="col">DrivingLicense</th>
            <th scope="col">ExpiryDate</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @if ($i==0)
            <tr>
                <td colspan="6"> No Data </td>
            </tr>
        @else   
            @foreach($drivers as $driver)
                <tr>
                    <th scope="row"> {{$i++}} </th>
                    <td>{{$driver->name}}</td>
                
                    <td>
                        {{$driver->ICno}}
                    </td>
                    <td>
                        {{$driver->HPno}}
                    </td>
                    <td>
                        {{$driver->drivingLicenseNo}}
                    </td>
                    <td>
                        {{$driver->expiryDate}}
                    </td>
                    <td>
                        
                        <ion-icon name="trash"></ion-icon>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection


