@extends('layouts.main')

@section('content')
    <h1>Owner's Details</h1>

    <div class="card">
        <h5 class="card-header">{{$owner->name}}</h5>
        <div class="card-body">
            <h5 class="card-title"><ion-icon name="mail"></ion-icon>{{$owner->email}}</h5>
            <!--<h5 class="card-title"><ion-icon name="phone-portrait"></ion-icon>{{$owner->HPno}}</h5>-->
            <a href="{{route('owner.edit',$owner->id)}}" class="btn btn-primary">Edit</a>
            <a href="{{url('/owner')}}" class="btn btn-warning">Back</a>
        </div>
    </div>
@endsection