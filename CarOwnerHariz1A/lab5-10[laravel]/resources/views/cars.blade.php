@extends('layouts.main')

@section('content')

    <h1>Cars List</h1>
    <!--<a class="btn btn-outline-primary float-right"><ion-icon name="person-add"></ion-icon></a>-->
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">RegNum</th>
            <th scope="col">Manufacturer</th>
            <th scope="col">Model</th>
            <th scope="col">YearMade</th>
            <th scope="col">Owner</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @if ($i==0)
            <tr>
                <td colspan="6"> No Data </td>
            </tr>
        @else   
            @foreach($cars as $car)
                <tr>
                    <th scope="row"> {{$i++}} </th>
                    <td>{{$car->RegNum}}</td>
                    <td>
                        
                        {{$car->Manufacturer}}
                        
                    </td>
                        
                    <td>
                        {{$car->Model}}
                    </td>
                    <td>
                        {{$car->YearMade}}
                    </td>
                    <td>
                        {{$car->Owner['name']}}
                    </td>
                    <td>
                        <a href="{{route('car.edit', $car->id)}}"><ion-icon name="create"></ion-icon></a>
                        <a href="{{route('destroy2', $car->id)}}"><ion-icon name="trash"></ion-icon></a>
                        
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection


