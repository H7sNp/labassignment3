@extends('layouts.main')

@section('content')
    <h1>Update Car Info</h1>
    <form action="{{route('car.update',$car->id)}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="RegNum">Reg Number</label>
            <input  type="text" name="RegNum" value="{{$car->RegNum}}" class="form-control">
        </div>

        <div class="form-group">
            <label for="Manufacturer">Manufacturer</label>
            <input  type="text" name="Manufacturer" value="{{$car->Manufacturer}}" class="form-control">
        </div>

        <div class="form-group">
            <label for="Model">Model</label>
            <input  type="text" name="Model" value="{{$car->Model}}" class="form-control">
        </div>


        <div class="form-group">
            <label for="YearMade">Year Made</label>
            <input  type="text" name="YearMade" value="{{$car->YearMade}}" class="form-control">
        </div>

        <div class="form-group">
            <label for="Owner">Owner</label>
            <select name="owner_id" class="custom-select">
                @if($car->owner !=null)
                    <option selected value="{{$car->owner_id}}">{{$car['owner']->name}}</option>
                @else 
                    <option selected value="">Please Select</option>
                @endif
                @foreach($owners as $owner)
                    <option value="{{$owner->id}}">{{$owner->name}}</option>
                @endforeach
                
                <option value="">-- No Owner --</option>
            </select>
        </div>

        <div class="form-group">
            <label for="Driver">Driver</label>
            @forelse($car->drivers as $d)
                <li>{{$d->name}}</li>
            @empty  
                <p>No Driver</p>
            @endforelse
            <select name="driver[]" class="custom-select" multiple>
            
                @foreach($drivers as $driver)
                    <option value="{{$driver->id}}">{{$driver->name}}</option>
                @endforeach
                
                <option value="">-- No Owner --</option>
            </select>

        </div>



        <input type="submit" class="btn btn-primary" values="Save">
        <a class="btn btn-warning" href="/car">Cancel</a>
    </form>
    <br>
@endsection
