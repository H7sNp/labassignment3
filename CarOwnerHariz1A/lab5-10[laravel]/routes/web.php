<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::resource('/owner','OwnerController');//->middleware('auth');

Route::resource('/car','CarController');
/////////////////////////////////////////////
//lab6
///////////////////
Route::resource('/driver','DriverController');

Route::get('/alldriver','DriverController@index');

Route::post('/owner/create','OwnerController@store');

Route::get('/delowner/{owner}','OwnerController@destroy')->name('destroy1');
Route::get('/delowner/{car}','CarController@destroy')->name('destroy2');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');




