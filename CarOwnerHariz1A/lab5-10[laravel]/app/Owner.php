<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    protected $fillable = ['id', 'name','email'];

    public function cars()
    {
        return $this->hasMany(Car::class);
    }
}



