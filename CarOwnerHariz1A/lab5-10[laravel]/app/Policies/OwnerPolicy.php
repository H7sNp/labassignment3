<?php

namespace App\Policies;

use App\Owner;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OwnerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any owners.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the owner.
     *
     * @param  \App\User  $user
     * @param  \App\Owner  $owner
     * @return mixed
     */
    public function view(User $user, Owner $owner)
    {
        //
    }

    /**
     * Determine whether the user can create owners.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the owner.
     *
     * @param  \App\User  $user
     * @param  \App\Owner  $owner
     * @return mixed
     */
    public function update(User $user, Owner $owner)
    {
        //
    }

    /**
     * Determine whether the user can delete the owner.
     *
     * @param  \App\User  $user
     * @param  \App\Owner  $owner
     * @return mixed
     */
    public function delete(User $user, Owner $owner)
    {
        return $user->userLevel >=1;
    }

    /**
     * Determine whether the user can restore the owner.
     *
     * @param  \App\User  $user
     * @param  \App\Owner  $owner
     * @return mixed
     */
    public function restore(User $user, Owner $owner)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the owner.
     *
     * @param  \App\User  $user
     * @param  \App\Owner  $owner
     * @return mixed
     */
    public function forceDelete(User $user, Owner $owner)
    {
        //
    }
}
