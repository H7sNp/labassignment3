<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = ['RegNum','Manufacturer','Model','YearMade','owner_id'];

    public function owner()
    {
        return $this->belongsTo('App\Owner');
    }

    public function drivers()
    {
        return $this->belongsToMany('App\Driver','car_driver');
    }
}
