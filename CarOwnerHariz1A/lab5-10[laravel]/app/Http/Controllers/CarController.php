<?php

namespace App\Http\Controllers;

use App\Car;
use App\Owner;
use App\Driver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$car = Car::all();
        //$car2 = Car::with('owner')->get(); //skali dgn info owners
        //return $car2;

        //$cars=Car::where('Manufacturer','Proton')->get();
        //return $cars;

        //$cars=Auth::user()->email;
        //return $cars;

        $cars = Car::with('owner')->get();
        

        $i=0;
        if($cars->count()>0)
            $i=1;
            
        return view('cars',compact('cars','i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        return view('showcars',$car);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        //$C = Car::with('owner')->get();

       // return $car;
       

        $owners= Owner::all();
        $drivers=Driver::all();
        return view('editcar',compact('car','owners','drivers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        $car->update($request->all());
        

        $car->drivers()->sync($request->get('driver'));
        
        return redirect()->route('car.index')->with('success','Car List Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        $car->delete();
        return redirect()->route('car.index')->with('delsuccess','Car Deleted!');
    }
}
