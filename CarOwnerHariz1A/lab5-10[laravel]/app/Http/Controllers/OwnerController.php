<?php

namespace App\Http\Controllers;

use App\Owner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; //untuk auth
use Illuminate\Support\Facades\Gate;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //LAB 5
        //$owner = Owner::all(); //display semua info
        //$owner2 = Owner::with('cars')->get(); //skali dgn info cars
        //return $owner2;

        //LAB 6

        $owners = Owner::with('cars')->get();

        $i=0;
        if($owners->count()>0)
            $i=1;
        
        /*if(Auth::check()
        {
            return view('owner',compact('owners','i'));
        }
        else
        {
            return vie('home');
        }*/

        //kat blade buat yg @guest tu.

        return view('owner',compact('owners','i'));
        

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $owner= new \App\Owner;
        $owner->name= $request->get('name');
        $owner->email= $request->get('email');
        $owner->save();
        return redirect('/owner')->with('success','New ownerr Added/ this message ade kat controller');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function show(Owner $owner)
    {
        return view('showowner',compact('owner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function edit(Owner $owner)
    {
        
        $cars = $owner->cars;
        if(Gate::allows('edit',$cars))
        {
             return view('editowner',compact('owner','cars'));
        }
        else
        {
            echo '<h2> Un-Authorized Access!</h2>';
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Owner $owner)
    {
        $owner->update($request->all());
        return redirect()->route('owner.index')->with('success','Owner Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Owner $owner)
    {
        $user=Auth::user();

        if($user->can('delete',$owner))
        {
            $owner->delete();
            return redirect()->route('owner.index')->with('delsuccess','Owner Deleted!');
        }
        else
        {
            echo '<h2> Un-Authorized Access!</h2>';
        }
       
    }
}
